export default interface Track{
  Id:  number;
  url: string;
  owner: string;
  bpm: number;
  isPlaying?: boolean;
  volume?: number;
}