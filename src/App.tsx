import React, {Component} from 'react';
import './App.css';
import Button from 'src/components/input/Button';
import Player from 'src/components/Player';
import Track from 'src/types/track';
import data from "src/sample/data.json";
import {Howl} from 'howler';

interface State {
  trackList: Map<number, Track>,
  looperList: Map<number, Track>,
  loading: boolean,
  isPlayingAll: boolean,
  isPlaying: boolean
}

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class App extends Component<{}, State> {
  state = {
    trackList: new Map(),
    looperList: new Map(),
    loading: true,
    isPlayingAll: false,
    isPlaying: false
  }

  player: Howl | null = null;
  looperPlayer: Howl[] = [];
  loadedTrack: Track | null = null;

  componentDidMount = () => {
    //Simulates tracks coming from API
    const map = new Map();
    data.forEach(track => map.set(track.Id, track));
    this.setState({trackList: map});
  }

  /**
   * @description Moves an element from trackList to looperList given it's index in trackList
   * @param index index of Item in trackList to be added to looperList
   */
  addToLooper = (index: number) => {
    const {trackList, looperList} = this.state;
    const track: Track = trackList.get(index);
    trackList.delete(index);
    looperList.set(index, track);
    
    this.looperPlayer[index] = new Howl({
      src: [track.url],
      html5: true,
      loop: true
    });
    
    this.setState({trackList: trackList, looperList: looperList})
  }

  /**
   * @description Moves an element from looperList to trackList given it's index in looperList
   * @param index index of Item in looperList to be added to trackList
   */
  removeFromLooper = (index: number) => {
    const {trackList, looperList} : {trackList: Map<number, Track>, looperList: Map<number, Track>} = this.state;
    const track: Track | undefined = looperList.get(index);
    
    looperList.delete(index);
    
    this.looperPlayer[index].stop();
    delete this.looperPlayer[index];

    if(track) {
      track.isPlaying = false;
      trackList.set(index, track);
    }
    this.setState({looperList: looperList, trackList: trackList})
  }

  /**
   * @description Plays the track on audio channel and it updates the UI to match the state
   * @param track Track to be played
   */
  playTrack = async(track: Track) => {
    this.pauseAll();

    this.updateTrack(track, {...track, isPlaying: true});

    if(this.loadedTrack?.Id !== track.Id ){
      this.player = new Howl({
        src: [track.url],
        html5: true,
        volume: track.volume && (track.volume / 100)
      })
      
      this.loadedTrack = track;
    }

    this.player?.play();

    this.setState({isPlaying: true}, async() =>{
      while(this.state.isPlaying){
        await sleep(100);
        this.updateProgression();
      }}
    );
  }

  /**
   * @description Pauses the track on audio channel and it updates the UI to match the state
   * @param track Track to be paused
   */
  pauseTrack = (track: Track) => {
    this.updateTrack(track, {...track, isPlaying: false})

    this.player?.pause();
    this.looperPlayer.forEach(howl => howl.stop());
    this.setState({isPlaying: false});
    this.loadedTrack = null;
  }

  /**
   * @description Plays all the tracks in looperList
   */
  playAll = () => {
    const {looperList}: {looperList: Map<number, Track>} = this.state;

    this.pauseAll();
    
    this.looperPlayer.forEach((howl, index: number) => {
      const track: Track | undefined = looperList.get(index);
      if(track && track.volume) howl.volume(track.volume / 100);
      howl.play();
    })

    looperList.forEach((value, key) => {
      looperList.set(key, {...value, isPlaying: true})
    })

    this.setState({looperList: looperList, isPlayingAll: true, isPlaying: false}, async() =>{
      while(this.state.isPlayingAll){
        await sleep(100);
        this.updateProgression();
      }}
    );
  }

  /**
   * @description Pauses all the tracks in looperList and all indipendt track playing
   */
  pauseAll = () => {
    const {trackList, looperList} : {trackList: Map<number, Track>, looperList: Map<number, Track>} = this.state;
    
    looperList.forEach((value, key) => {
      looperList.set(key, {...value, isPlaying: false})
    })

    trackList.forEach((value, key) => {
      trackList.set(key, {...value, isPlaying: false})
    })

    this.player?.pause();
    this.looperPlayer.forEach(howl => howl.stop());
    this.loadedTrack = null;

    this.setState({trackList: trackList, looperList: looperList, isPlayingAll: false, isPlaying: false});
  }

  /**
   * @description Sets the volume of a track whether it's playing or not
   * @param track The track to be modified
   * @param volume The volum amount to be setted
   */
  updateTrackVolume = (track: Track, volume: number) => {
    this.updateTrack(track, {...track, volume});

    if(this.loadedTrack?.Id === track.Id){
      this.player?.volume(volume / 100);
    }
  }

  /**
   * @description Sets the track to be newTrack
   * @param track Track to be modified
   * @param newTrack Track to be setted
   */
  updateTrack = (track: Track, newTrack: Track) => {
    const {trackList, looperList} : {trackList: Map<number, Track>, looperList: Map<number, Track>} = this.state;

    const trackValue: Track | undefined = trackList.get(track.Id)
    if(!trackValue) {
      looperList.set(track.Id, newTrack);
      this.setState({looperList});
      if(newTrack.volume && this.looperPlayer[track.Id]) this.looperPlayer[track.Id].volume(newTrack.volume / 100);
    }
    else{
      trackList.set(track.Id, newTrack);
      this.setState({trackList});
    }
  }
  
  updateProgression = () => {
    
    this.forceUpdate();
    
  }

  render() {
    const {trackList, looperList, isPlayingAll} = this.state;
    const trackArr = Array.from(trackList.values());
    const looperArr = Array.from(looperList.values());
    
    return (
      <div className="App">
      <header>
        <Button label="Sync" onClick={() => console.log("Sync")} border/>
        {!isPlayingAll && <Button label="Play" onClick={this.playAll}/>}
        {isPlayingAll && <Button label="Pause" onClick={this.pauseAll}/>}
      </header>
      <p className="subtitle">Looper</p>
      {looperArr.map((track: Track) => (
        <Player
          trackName={track.owner}
          trackUrl={track.url}
          isPlaying={track.isPlaying}
          bpm={track.bpm}
          key={track.Id}
          mode="remove"
          onClick={() => this.removeFromLooper(track.Id)}
          onPlay={() => this.playTrack(track)}
          onPause={() => this.pauseTrack(track)}
          onVolume={(volume: number) => this.updateTrackVolume(track, volume)}
          progress={this.loadedTrack?.Id === track.Id ? parseInt(JSON.stringify(this.player?.seek())) : this.looperPlayer[track.Id].playing() ? parseInt(JSON.stringify(this.looperPlayer[track.Id]?.seek())) : undefined}
        />
      ))}
      <p className="subtitle">Select Track</p>
      {trackArr.map((track: Track) => (
        <Player
          trackName={track.owner}
          trackUrl={track.url}
          isPlaying={track.isPlaying}
          bpm={track.bpm}
          key={track.Id}
          mode="add"
          onClick={() => this.addToLooper(track.Id)}
          onPlay={() => this.playTrack(track)}
          onPause={() => this.pauseTrack(track)}
          onVolume={(volume: number) => this.updateTrackVolume(track, volume)}
          progress={this.loadedTrack?.Id === track.Id ? parseInt(JSON.stringify(this.player?.seek())) : undefined}
        />
      ))}
    </div>
    )
  }
}

export default App;
