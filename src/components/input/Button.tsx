import React, { MouseEventHandler } from 'react'
import style from "src/styles/button.module.scss";

interface Props {
  label: string,
  onClick: MouseEventHandler<HTMLButtonElement>,
  border?: boolean
}

const Button: React.FC<Props> = ({label, onClick, border = false}) => {
  return (
    <button onClick={onClick} className={`${style.button} ${border && style.border}`}>
      {label}
    </button>
  )
}

export default Button
